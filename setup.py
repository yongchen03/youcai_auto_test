#!/usr/bin/python

"""
Package Installation
"""

import re
import os
from setuptools import setup, find_packages
from glob import glob

requires = ['requests==2.7.0',
            'greenlet==0.4.9',
            'grequests==0.2.0',
            'mysql-connector-python==2.0.4',
            'pymongo',
            'pytest==2.9.1',
            'sqlalchemy',
            'pika',
            'paramiko',
            'argcomplete',
            'termcolor',
            'flask',
            'flask-cors',
            'flask-sqlalchemy',
            'flask-restful',
            'passlib',
            'itsdangerous',
            'flask-httpauth',
            'rsa',
            'jsonschema',
            'openpyxl==2.3.5']

#Data Config--find the files if exist, and delete them
files = [('/Users/chenyong/PycharmProjects', glob('test1/*') + glob('data_tmp/*'))]



