import requests
from bin.http_preparation import require_header_with_token
from bin.http_preparation import require_header_without_token
from bin.http_preparation import require_supplier_without_token
from bin.http_preparation import require_supplier_with_token


def product_category():
    url = 'http://api.youcai.beta.elenet.me/restaurantapi/restaurantgate/product/category'
    response = requests.get(url)
    return response.text


@require_header_with_token
def order_restaurant_count(**kwargs):
    url = 'http://api.youcai.beta.elenet.me/restaurantapi/restaurantgate/order/restaurant_count'
    response = requests.get(url, headers=kwargs)
    return response.text


@require_header_without_token
def mobile_login(**kwargs):
    url = 'http://api.youcai.beta.elenet.me/restaurantapi/restaurantgate/sso/mobilelogin'
    payload = {"app":2,"vericode":"517517","mobile":"13829987357","captcha":"1104","tick":"b657414d-16e1-4740-9f49-36ac63a22e04"}
    response = requests.post(url, headers=kwargs, json=payload)
    return response.text

@require_supplier_without_token
def supplier_mobile_login(**kwargs):
    url = 'http://api.youcai.beta.elenet.me/sso/mobilelogin'
    payload = {"app":1,"vericode":"517517","mobile":"18888888888","captcha":"1104","tick":"463bca79-f365-40e2-9658-dd447e245c26"}
    response = requests.post(url, headers=kwargs, json=payload)
    return response.text


@require_supplier_with_token
def supplier_info(**kwargs):
    url = 'http://api.youcai.beta.elenet.me:80/supplier/45'
#    payload = {"app":1,"vericode":"517517","mobile":"18888888888","captcha":"1104","tick":"463bca79-f365-40e2-9658-dd447e245c26"}
    response = requests.get(url, headers=kwargs)
    return response.text