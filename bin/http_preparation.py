def require_header_with_token(func):
    def inner(*args, **kwargs):
        kwargs['Host'] = 'api.youcai.beta.elenet.me'
        kwargs['x-token'] = '9e98703f6c36bbcaf94df0b5c683276ba0c4ec4fbe269bbe538ce91f8008b4a6'
        kwargs['Content-Type'] = 'application/json'
        return func(*args, **kwargs)
    return inner


def require_header_without_token(func):
    def inner(*args, **kwargs):
        kwargs['Host'] = 'api.youcai.beta.elenet.me'
        kwargs['Content-Type'] = 'application/json'
        return func(*args, **kwargs)
    return inner

def require_supplier_without_token(func):
    def inner(*args, **kwargs):
        kwargs['Host'] = 'api.youcai.beta.elenet.me'
        kwargs['Content-Type'] = 'application/json'
        kwargs['Accept-Encoding'] = 'gzip, deflate'
        kwargs['User-Agent'] = 'YouCaiApp/me.ele.YouCaiRestaurant AppVersion/17 Platform/IOS ID/96BE3D9C-707C-477C-BF8E-F1C2059CA365 Channel/Pgyer Longitude/121.381640 Latitude/31.232800'
        return func(*args, **kwargs)
    return inner

def require_supplier_with_token(func):
    def inner(*args, **kwargs):
        kwargs['Host'] = 'api.youcai.beta.elenet.me'
        kwargs['Content-Type'] = 'application/json'
        kwargs['Accept-Encoding'] = 'gzip, deflate'
        kwargs['x-token'] = '1dc7d73f83b7f09b604226d206a3732eb00dc13532f57cd2f7e763455eeb5576'
        kwargs['User-Agent'] = 'YouCaiApp/me.ele.YouCaiRestaurant AppVersion/17 Platform/IOS ID/96BE3D9C-707C-477C-BF8E-F1C2059CA365 Channel/Pgyer Longitude/121.381640 Latitude/31.232800'
        return func(*args, **kwargs)
    return inner